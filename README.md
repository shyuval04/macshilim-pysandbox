# Macshilim PySandBox

Basically, it is a challenge i created and planned to publish it to magshimim as a bonus challenge,
but for unclear reason i didnt really get to send it to them.

the challenge is has actually 2 files, the sandbox and the folder generator.

the folder generator file creates lots of folders one within each other, and hides the file in one of the inner folders. it creates around 3000 folders in around a minute or two/

the second file the challenge itself, the pysandbox, where the challengers need to gain information about the box and "shut it down" to win the challenge.

my original planning was to buy an ssh server for that. but you can feel free and try for yourself the challenge.